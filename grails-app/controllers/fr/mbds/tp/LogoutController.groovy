package fr.mbds.tp

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils


class LogoutController {
    SpringSecurityService springSecurityService
    def index = {
        def user = springSecurityService.getCurrentUserId()
        user = User.get(user)
        user.dateLastConnexion = new Date().toTimestamp()
        user.save(flush: true, failOnError: true)
        redirect url: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl
    }
}
