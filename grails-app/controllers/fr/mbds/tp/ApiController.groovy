package fr.mbds.tp

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured("ROLE_ADMIN")
class ApiController {

    ApiService apiService

    def index() {
        render "ok"
    }

    def game() {
        switch (request.getMethod()) {
            case "POST":
                if(request.JSON.winner == null){
                    render(status: 422, text: '{"what" : "winner", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.looser == null) {
                    render(status: 422, text: '{"what" : "looser", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.winnerScore == null) {
                    render(status: 422, text: '{"what" : "winnerScore", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.looserScore == null) {
                    render(status: 422, text: '{"what" : "looserScore", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User looser = apiService.getUser(request.JSON.looser)
                User winner = apiService.getUser(request.JSON.winner)

                if(looser == null) {
                    render(status: 404, text: '{"what" : "looser", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }else if(winner == null){
                    render(status: 404, text: '{"what" : "winner", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if (!(request.JSON.looserScore instanceof Integer)){
                    render(status: 400, text: '{"what" : "looserScore", "why" : "wrong type"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(!(request.JSON.winnerScore instanceof Integer)){
                    render(status: 400, text: '{"what" : "winnerScore", "why" : "wrong type"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                Game game = new Game()
                game.setLooser(looser)
                game.setWinner(winner)
                game.setLooserScore(request.JSON.looserScore)
                game.setWinnerScore(request.JSON.winnerScore)
                int idGame = apiService.postGame(game)

                if(idGame != false){
                    render(status: 201, text: '{"what" : "success", "id" : "' + idGame + '"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                break
            case "GET":
                if (params.id != null) {
                    Game myGame = apiService.getGame(Long.parseLong(params.id))

                    if (myGame != null) {
                        render(status: 200, text: myGame as JSON)
                        return
                    } else {
                        render(status: 404, text: '{"what" : "error", "why" : "game not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break
            case "PUT":
                if (request.JSON.game_id != null) {
                    Game game = Game.findById(request.JSON.game_id)
                    if (game == null) {
                        render(status: 404, text: '{"what" : "error", "why" : "game not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                    if (request.JSON.looser != null) {
                        User looser = User.findById(request.JSON.looser)
                        if (looser == null) {
                            render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        game.setLooser(looser)
                    }
                    if (request.JSON.winner != null) {
                        User winner = User.findById(request.JSON.winner)
                        if (winner == null) {
                            render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        game.setLooser(winner)
                    }
                    if (request.JSON.looser_score != null) {
                        game.setLooserScore(request.JSON.looser_score)
                    }
                    if (request.JSON.winner_score != null) {
                        game.setWinnerScore(request.JSON.winner_score)
                    }
                    if (apiService.putGame(game)) {
                        render(status: 200, text: '{"what" : "success", "why" : "all good"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                    render(status: 500, text: '{"what" : "error", "why" : "game not updated"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                render(status: 405)
                return
                break
            case "DELETE":
                if (params.id != null) {
                    if (Integer.parseInt(params.id) < 1) {
                        render(status: 404, text: '{"what" : "error", "why" : "game not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else if (apiService.deleteGame(Long.parseLong(params.id))) {
                        render(status: 200, text: '{"what" : "success", "why" : "all good"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else {
                        render(status: 500, text: '{"what" : "error", "why" : "game not deleted"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break
            default:
                response.status = 405
                break
        }
    }

    def games() {
        switch (request.getMethod()) {
            case "POST":
                if(request.JSON.winner == null){
                    render(status: 422, text: '{"what" : "winner", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.looser == null) {
                    render(status: 422, text: '{"what" : "looser", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.winnerScore == null) {
                    render(status: 422, text: '{"what" : "winnerScore", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(request.JSON.looserScore == null) {
                    render(status: 422, text: '{"what" : "looserScore", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User looser = apiService.getUser(request.JSON.looser)
                User winner = apiService.getUser(request.JSON.winner)

                if(looser == null) {
                    render(status: 404, text: '{"what" : "looser", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }else if(winner == null){
                    render(status: 404, text: '{"what" : "winner", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if (!(request.JSON.looserScore instanceof Integer)){
                    render(status: 400, text: '{"what" : "looserScore", "why" : "wrong type"}', contentType: "text/json", encoding: "UTF-8")
                    return
                } else if(!(request.JSON.winnerScore instanceof Integer)){
                    render(status: 400, text: '{"what" : "winnerScore", "why" : "wrong type"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                Game game = new Game()
                game.setLooser(looser)
                game.setWinner(winner)
                game.setLooserScore(request.JSON.looserScore)
                game.setWinnerScore(request.JSON.winnerScore)
                int idGame = apiService.postGames(game)

                if(idGame != false){
                    render(status: 201, text: '{"what" : "success", "id" : "' + idGame + '"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                break
            case "GET":
                def myGames = apiService.getGames()

                if (myGames != null) {
                    render(status: 200, text: myGames as JSON)
                    return
                } else {
                    render(status: 404, text: '{"what" : "error", "why" : "game not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                render(status: 405)
                break
            default:
                response.status = 405
                break
        }
    }

    def message() {
        switch (request.getMethod()) {
            case "POST":
                if (request.JSON.author == null) {
                    render(status: 422, text: '{"what" : "author", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                if (request.JSON.target == null) {
                    render(status: 422, text: '{"what" : "target", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.content == null) {
                    render(status: 422, text: '{"what" : "content", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User author = apiService.getUser(request.JSON.author)
                User target = apiService.getUser(request.JSON.target)

                if (author == null) {
                    render(status: 404, text: '{"what" : "author", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (target == null) {
                    render(status: 404, text: '{"what" : "target", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                Message message = new Message()
                message.setAuthor(author)
                message.setTarget(target)
                message.setContent(request.JSON.content)

                def newId = apiService.postMessage(message)

                if (newId != false) {
                    render(status: 201, text: '{"what" : "success", "id" : "' + newId + '"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                render(status: 500, text: '{"what" : "error", "why" : "message not created"}', contentType: "text/json", encoding: "UTF-8")
                return
                break

            case "GET":
                if (params.id != null) {
                    Message myMessage = apiService.getMessage(Long.parseLong(params.id))

                    if (myMessage != null) {
                        render(status: 200, text: myMessage as JSON)
                        return
                    } else {
                        render(status: 404, text: '{"what" : "error", "why" : "message not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break

            case "PUT":
                if (request.JSON.message_id != null) {
                    Message message = Message.findById(request.JSON.message_id)
                    if (message == null) {
                        render(status: 404, text: '{"what" : "error", "why" : "message not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                    if (request.JSON.author != null) {
                        User author = User.findById(request.JSON.author)
                        if (author == null) {
                            render(status: 404, text: '{"what" : "error", "why" : "author not found"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        message.setAuthor(author)
                    }
                    if (request.JSON.target != null) {
                        User target = User.findById(request.JSON.target)
                        if (target == null) {
                            render(status: 404, text: '{"what" : "error", "why" : "target not found"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        message.setTarget(target)
                    }
                    if (request.JSON.content != null) {
                        message.setContent(request.JSON.content)
                    }

                    if (apiService.putMessage(message)) {
                        render(status: 200, text: '{"what" : "success", "why" : "all good"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                    render(status: 500, text: '{"what" : "error", "why" : "message not updated"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                render(status: 405)
                break

            case "DELETE":
                if (params.id != null) {
                    if (!Message.findById(Long.parseLong(params.id))) {
                        render(status: 404, text: '{"what" : "error", "why" : "message not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else if (apiService.deleteMessage(Long.parseLong(params.id))) {
                        render(status: 200, text: '{"what" : "success", "why" : "message deleted"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else {
                        render(status: 500, text: '{"what" : "error", "why" : "message not deleted"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break

            default:
                response.status = 405
                break
        }
    }

    def messages() {
        switch (request.getMethod()) {
            case "POST":
                if (request.JSON.author == null) {
                    render(status: 422, text: '{"what" : "author", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                if (request.JSON.target == null) {
                    render(status: 422, text: '{"what" : "target", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.content == null) {
                    render(status: 422, text: '{"what" : "content", "why" : "missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User author = apiService.getUser(request.JSON.author)
                User target = apiService.getUser(request.JSON.target)

                if (author == null) {
                    render(status: 404, text: '{"what" : "author", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (target == null) {
                    render(status: 404, text: '{"what" : "target", "why" : "not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                Message message = new Message()
                message.setAuthor(author)
                message.setTarget(target)
                message.setContent(request.JSON.content)

                def newId = apiService.postMessage(message)

                if (newId != false) {
                    render(status: 201, text: '{"what" : "success", "id" : "' + newId + '"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                render(status: 500, text: '{"what" : "error", "why" : "good question"}', contentType: "text/json", encoding: "UTF-8")
                return
                break

            case "GET":
                def myMessages = apiService.getMessages()

                if (myMessages != null) {
                    render(status: 200, text: myMessages as JSON)
                    return
                } else {
                    render(status: 404, text: "Messages not found")
                    return
                }
                render(status: 405)
                break
            default:
                response.status = 405
                break
        }
    }

    def user() {
        switch (request.getMethod()) {
            case "POST":
                if (request.JSON.username == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "username is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.password == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "password is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.roles == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "role is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.image == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "image is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.image_extension == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "image extension is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User user = new User()
                user.setUsername(request.JSON.username)
                user.setPassword(request.JSON.password)

                def userNewId = apiService.postUser(user, request.JSON.image, request.JSON.image_extension)
                if (userNewId == null || userNewId == false) {
                    render(status: 500, text: '{"what" : "error", "why" : "enable to save user"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                for (String role : request.JSON.roles) {
                    if (!apiService.postRole(userNewId, role)) {
                        render(status: 500, text: '{"what" : "error", "why" : "enable to save user-role"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }

                render(status: 201, text: '{"what" : "success", "id" : "' + userNewId + '"}', contentType: "text/json", encoding: "UTF-8")
                return
                break
            case "GET":
                if (params.id != null) {
                    User myUser = apiService.getUser(Long.parseLong(params.id))

                    if (myUser != null) {
                        render(status: 200, text: myUser as JSON)
                        return
                    } else {
                        render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break
            case "PUT":
                if (request.JSON.user_id != null) {
                    User user = User.findById(request.JSON.user_id)
                    if (user == null) {
                        render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                    if (request.JSON.password != null) {
                        user.setPassword(request.JSON.password)
                    }
                    if (request.JSON.username != null) {
                        User u = User.findByUsername(request.JSON.username)
                        if (u != null) {
                            render(status: 400, text: '{"what" : "error", "why" : "username already taken"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        user.setUsername(request.JSON.username)
                    }
                    if (request.JSON.roles != null) {
                        List<UserRole> ur = UserRole.findAllByUser(user)
                        apiService.putUserRemoveRoles(ur)
                        List<UserRole> userRoleList = new ArrayList<>()
                        for (String role : request.JSON.roles) {
                            UserRole userRole = new UserRole()
                            Role r = Role.findByAuthority(role)
                            userRole.setUser(user)
                            userRole.setRole(r)
                            userRoleList.add(userRole)
                        }

                        apiService.putUserRoles(userRoleList)
                    }
                    if (request.JSON.image != null) {
                        if (request.JSON.image_extension != null) {
                            if (apiService.putUserImage(user, request.JSON.image, request.JSON.image_extension)) {
                                render(status: 200, text: '{"what" : "success", "why" : "all good"}', contentType: "text/json", encoding: "UTF-8")
                                return
                            }
                            render(status: 500, text: '{"what" : "error", "why" : "message not updated"}', contentType: "text/json", encoding: "UTF-8")
                            return
                        }
                        render(status: 422, text: '{"what" : "error", "why" : "extension is missing"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }

                    if (apiService.putUser(user)) {
                        render(status: 200, text: '{"what" : "success", "why" : "all good"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }

                    render(status: 500, text: '{"what" : "error", "why" : "message not updated"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                render(status: 405)
                break

                break
            case "DELETE":
                if (params.id != null) {
                    if (!User.findById(Long.parseLong(params.id))) {
                        render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else if (apiService.deleteUser(Long.parseLong(params.id))) {
                        render(status: 200, text: '{"what" : "success", "why" : "user deleted"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    } else {
                        render(status: 500, text: '{"what" : "error", "why" : "user not deleted"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }
                render(status: 405)
                break
            default:
                response.status = 405
                break
        }
    }

    def users() {
        switch (request.getMethod()) {
            case "POST":
                if (request.JSON.username == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "username is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.password == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "password is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.roles == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "role is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.image == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "image is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                if (request.JSON.image_extension == null) {
                    render(status: 422, text: '{"what" : "error", "why" : "image extension is missing"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                User user = new User()
                user.setUsername(request.JSON.username)
                user.setPassword(request.JSON.password)

                def userNewId = apiService.postUser(user, request.JSON.image, request.JSON.image_extension)
                if (userNewId == null || userNewId == false) {
                    render(status: 500, text: '{"what" : "error", "why" : "not enable to save user"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }

                for (String role : request.JSON.roles) {
                    if (!apiService.postRole(userNewId, role)) {
                        render(status: 500, text: '{"what" : "error", "why" : "not enable to save user-role"}', contentType: "text/json", encoding: "UTF-8")
                        return
                    }
                }

                render(status: 201, text: '{"what" : "success", "id" : "' + userNewId + '"}', contentType: "text/json", encoding: "UTF-8")
                return
                break
            case "GET":
                def myUsers = apiService.getUsers()

                if (myUsers != null) {
                    render(status: 200, text: myUsers as JSON)
                    return
                } else {
                    render(status: 404, text: '{"what" : "error", "why" : "user not found"}', contentType: "text/json", encoding: "UTF-8")
                    return
                }
                render(status: 405)
                break
            default:
                response.status = 405
                break
        }
    }
}