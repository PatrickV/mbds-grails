package fr.mbds.tp

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import org.apache.commons.lang.RandomStringUtils

import static org.springframework.http.HttpStatus.*

@Secured("ROLE_ADMIN")
class UserController {

    UserService userService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def games = Game.findAll()
        def messages = Message.findAllByIsRead(false)
        respond userService.list(params), model:[userCount: userService.count(), userMessages: messages, userGames: games]
    }

    def show(Long id) {
        respond userService.get(id)
    }

    def create() {
        respond (new User(params), roleArray: Role.findAll())
    }

    def save(User user) {
        if (user == null) {
            notFound()
            return
        }

        try {
            userService.save(user)
        } catch (ValidationException e) {
            respond user.errors, view:'create'
            return
        }

        String imageUploadPath = grailsApplication.config.getProperty("filePath")
        File userPictureFolder = new File("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar)

        if (!userPictureFolder.exists()) {
            userPictureFolder.mkdir()
        }

        File userPicture = new File("${imageUploadPath}" + File.separatorChar + "tmp" + File.separatorChar + user.profilePicture)
        String newPictureName = userPicture.getName()
        if (userPicture.renameTo(new File(userPictureFolder, newPictureName))) {

            def roleFromForm = request.getParameterValues("roles-user")

            for (int i = 0; i < roleFromForm.size(); i++) {
                UserRole userRole = new UserRole(user: user, role: Role.findAllById(roleFromForm[i])[0])
                try {
                    userRole.save(flush: true, failOnError: true)
                } catch (ValidationException e) {
                    respond user.errors, view:'create'
                    return
                }
            }
        } else {
            respond user.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond userService.get(id)
    }

    def update(User user) {
        if (user == null) {
            notFound()
            return
        }

        try {
            userService.save(user)
        } catch (ValidationException e) {
            respond user.errors, view:'edit'
            return
        }

        List<UserRole> roles = UserRole.findAllByUser(user)

        for (UserRole r : roles) {
            r.delete(flush: true, failOnError: true)
        }

        def roleFromForm = request.getParameterValues("roles-user")

        for (int i = 0; i < roleFromForm.size(); i++) {
            UserRole userRole = new UserRole(user: user, role: Role.findAllById(roleFromForm[i])[0])
            try {
                userRole.save(flush: true, failOnError: true)
            } catch (ValidationException e) {
                respond user.errors, view:'create'
                return
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*'{ respond user, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

//        userService.delete(id)

        User user = User.findById(id)
        user.enabled = false
        if (user.save(flush: true, failOnError: true)) {
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: OK }
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def changePicture() {
        def user = User.findByUsername(request.getParameter("user"))
        def path = request.getParameter("newUrl")

        if (user != null) {
            user.profilePicture = path
            if (user.save(flush: true, failOnError: true)) {
                render(text: "{\"path\": \"" + path + "\"}", contentType: "text/json", encoding: "UTF-8")
            } else {
                render(text: "{\"error\": \"Unable to save the user\"}", contentType: "text/json", encoding: "UTF-8")
            }
        } else {
            render(text: "{\"error\": \"Unable to find the user\"}", contentType: "text/json", encoding: "UTF-8")
        }
    }

    def uploadImage(){

        def file = request.getFile('image')
        String imageUploadPath = grailsApplication.config.getProperty("filePath")
        try{
            if(file && !file.empty){

                String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
                Integer length = 32
                String randomString = RandomStringUtils.random(length, charset.toCharArray())

                String[] fullName = file.getOriginalFilename().tokenize('.')
                String extension = fullName[fullName.size() - 1]

                def fileNewPath = randomString + ".${extension}"
                def folder = "tmp"

                if (request.getParameter('username') != null && request.getParameter("username") != "") {
                    folder = request.getParameter('username')
                } else {
                    folder = "tmp"
                }
                file.transferTo(new File("${imageUploadPath}" + File.separatorChar + "${folder}" + File.separatorChar + "${fileNewPath}"))
                render(text: "{\"path\": \"" + fileNewPath + "\"}", contentType: "text/json", encoding: "UTF-8")
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
        }
        catch(Exception e){
            log.error("L'image n'a pas pu être enregistrée !",e)
        }

    }
}
