// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {

    (function($) {
        $(document).ajaxStart(function() {
            $('#spinner').fadeIn();
        }).ajaxStop(function() {
            $('#spinner').fadeOut();
        });
    })(jQuery);

    $(function() {

        var pathname = window.location.pathname;
        var splitedPath = pathname.split('/');
        switch (splitedPath[2]) {
            case 'user':
                if (splitedPath[3] == 'index') {
                    $('#user-list').addClass("current");
                } else if (splitedPath[3] == 'create') {
                    $('#user-create').addClass("current");
                }
                break;
            case 'message':
                if (splitedPath[3] == 'index') {
                    $('#message-list').addClass("current");
                } else if (splitedPath[3] == 'create') {
                    $('#message-create').addClass("current");
                }
                break;
            case 'game':
                if (splitedPath[3] == 'index') {
                    $('#game-list').addClass("current");
                } else if (splitedPath[3] == 'create') {
                    $('#game-create').addClass("current");
                }
                break;
        }

        $('#profile-image').on('click', function() {
            $('#profile-image-upload').click();
        });

        $("#user-delete").on('click', function () {

            var url = "/tp/user/delete/" + $("#user-id").val();

            $.ajax({
                type: 'DELETE',
                url: url,
                success: function () {
                    window.location = "/tp/user/index";
                }
            });

        });
    });
}
