$(function () {

    $("#profilePicture").parent("div").hide();
    $("#profilePicture").removeAttr('required');

    $("#profile-image-upload").change(function() {
        readURL($(this)[0])
        upload($(this)[0].files[0])
    });

    $(document).on('dragenter', '#dropfile', function() {
        $(this).css('border', '3px dashed red');
        return false;
    });

    $(document).on('dragover', '#dropfile', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).css('border', '3px dashed red');
        return false;
    });

    $(document).on('dragleave', '#dropfile', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).css('border', '3px dashed #BBBBBB');
        return false;
    });

    $(document).on('drop', '#dropfile', function(e) {
        if(e.originalEvent.dataTransfer){
            if(e.originalEvent.dataTransfer.files.length) {
                // Stop the propagation of the event
                e.preventDefault();
                e.stopPropagation();
                $(this).css('border', '3px dashed green');
                // Main function to upload
                readURL(e.originalEvent.dataTransfer)
               upload(e.originalEvent.dataTransfer.files);
            }
        }
        else {
            $(this).css('border', '3px dashed #BBBBBB');
        }
        return false;
    });
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#profile-image').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function upload(files) {
    var img = files[0];
    var form = new FormData();
    var username = $("#username-show").val();
    if (username == undefined) {
        username = "";
    }

    form.append("image", files[0]);
    form.append("username", username);
    $.ajax({
        type: 'POST',
        url: '/tp/user/uploadImage',
        contentType: false,
        cache: false,
        processData:false,
        data: form,
        dataType: 'json',
        complete: function (data) {
            jsonResponse = JSON.parse(data.responseText)
            if (jsonResponse.hasOwnProperty("path")) {
                $.ajax({
                    type: 'POST',
                    url: '/tp/user/changePicture',
                    data: {"newUrl": jsonResponse.path, "user": $("#username-show").val()},
                    dataType: 'json',
                    complete: function (data) {

                    }
                })
            } else {
                alert("Une erreur est survenue")
            }
        }
    });
}