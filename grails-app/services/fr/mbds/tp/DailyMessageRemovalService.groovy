package fr.mbds.tp

import grails.gorm.transactions.Transactional
import org.springframework.scheduling.annotation.Scheduled

@Transactional
class DailyMessageRemovalService {

    boolean lazyInit = false

    MessageService messageService

    @Scheduled(cron = "0 0 4 1/1 * ?")
    void execute() {
        def messages = Message.findAllByIsRead(Boolean.TRUE)
        if (messages.size() > 0) {
            messages.each {
                messageService.delete(it.id)
                println("Message between " + it.author.username + " and " + it.target.username + " has been removed!")
            }
        }
    }
}
