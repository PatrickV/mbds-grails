package fr.mbds.tp

import grails.gorm.transactions.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class ApiService {
    def grailsApplication

    def serviceMethod() {

    }

    // GET

    def getGame(Long id) {
        return Game.get(id)
    }

    def getGames() {
        return Game.findAll()
    }

    def getMessage(Long id) {
        return Message.get(id)
    }

    def getMessages() {
        return Message.findAll()
    }

    def getUser(Long id) {
        return User.get(id)
    }

    def getUsers() {
        return User.findAllByEnabled(true)
    }

    // POST
    def postGame(Game game){
        if(game.save(flush: true, failOnError: true)){
            return game.id
        }
        return false
    }

    def postUser(User user) {
        if(user.save(flush: true, failOnError: true)){
            return user.id
        }
        return false
    }

    def postMessage(Message message) {
        if (message.save(flush: true, failOnError: true)) {
            return message.id
        }
        return false
    }

    def postGames(Game game){
        if(game.save(flush: true, failOnError: true)){
            return game.id
        }
        return false
    }

    def postRole(Long userId, String role) {

        User u = User.findById(userId)
        Role r = Role.findByAuthority(role)

        UserRole ur = new UserRole()
        ur.setUser(u)
        ur.setRole(r)

        if (ur.save(flush: true, failOnError: true)) {
            return true
        }
        return false
    }

    def postUser(User user, String image, String extension) {

        if (!User.findByUsername(user.getUsername())) {

            String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
            Integer length = 32
            String randomString = RandomStringUtils.random(length, charset.toCharArray())

            String imageUploadPath = grailsApplication.config.getProperty("filePath")

            File folder = new File("${imageUploadPath}" + File.separatorChar + "${user.username}")
            if (!folder.exists()) {
                if (!folder.mkdir()) {
                    return false
                }
            }

            File file = new File("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar + randomString + "." + extension)
            if (!file.createNewFile()) {
                return false
            }

            FileOutputStream fos = new FileOutputStream("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar + randomString + "." + extension)
            fos.write(Base64.getDecoder().decode(image))
            fos.close()

            user.setProfilePicture(randomString)

            if (user.save(flush: true, failOnError: true)) {
                return user.id
            }
        }

        return false
    }


    // PUT

    def putGame(Game game) {
        if (game.save(flush: true, failOnError: true)) {
            return true
        }
        return false
    }

    def putMessage(Message message) {
        if (message.save(flush: true, failOnError: true)) {
            return true
        }
        return false
    }

    def putUserRemoveRoles(List<UserRole> userRoles) {
        for (UserRole ur : userRoles) {
            ur.delete(flush: true, failOnError: true)
        }
    }

    def putUserRoles(List<UserRole> userRoles) {
        for (UserRole ur : userRoles) {
            ur.save(flush: true, failOnError: true)
        }
    }

    def putUser(User user) {
        if (user.save(flush: true, failOnError: true)) {
            return true
        }
        return false
    }

    def putUserImage(User user, String image, String extension) {

        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        Integer length = 32
        String randomString = RandomStringUtils.random(length, charset.toCharArray())

        String imageUploadPath = grailsApplication.config.getProperty("filePath")

        File folder = new File("${imageUploadPath}" + File.separatorChar + "${user.username}")
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                return false
            }
        }

        File oldFile = new File("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar + user.getProfilePicture())
        oldFile.delete()

        File file = new File("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar + randomString + "." + extension)
        if (!file.createNewFile()) {
            return false
        }

        FileOutputStream fos = new FileOutputStream("${imageUploadPath}" + File.separatorChar + "${user.username}" + File.separatorChar + randomString + "." + extension)
        fos.write(Base64.getDecoder().decode(image))
        fos.close()

        user.setProfilePicture(randomString)

        if (user.save(flush: true, failOnError: true)) {
            return user.id
        }

        return false
    }



    // DELETE

    def deleteGame(Long id) {
        Game game = Game.get(id)
        if (game != null) {
            println("-------------->")
            game.delete(flush: true, failOnError: true)
            return true
        }
        return false
    }

    def deleteMessage(Long id) {
        Message message = Message.get(id)
        if (message != null) {
            message.delete(flush: true, failOnError: true)
            return true
        }
        return false
    }

    def deleteUser(Long id) {
        User user = User.get(id)
        if (user != null) {
            user.enabled = false
            user.save(flush: true, failOnError: true)
            return true
        }
        return false
    }
}
