package gestionutilisateurs

import fr.mbds.tp.Game
import fr.mbds.tp.Message
import fr.mbds.tp.Role
import fr.mbds.tp.User
import fr.mbds.tp.UserRole

class BootStrap {

    def init = { servletContext ->

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true, failOnError: true)
        def playerRole = new Role(authority: 'ROLE_USER').save(flush: true, failOnError: true)

        def adminUser = new User(username: 'admin', password: 'password', profilePicture: 'wallhaven-251579.jpg').save(flush: true, failOnError: true)
        def playerUser = new User(username: 'player', password: 'password', profilePicture: 'wallhaven-554217.jpg').save(flush: true, failOnError: true)
        def admin2User = new User(username: 'admin2', password: 'password', profilePicture: 'wallhaven-251579.jpg').save(flush: true, failOnError: true)
        def player2User = new User(username: 'player2', password: 'password', profilePicture: 'wallhaven-554217.jpg').save(flush: true, failOnError: true)

        UserRole.create(adminUser, adminRole, true)
        UserRole.create(playerUser, playerRole, true)

        UserRole.create(admin2User, adminRole, true)
        UserRole.create(player2User, playerRole, true)

        new Game(winner: admin2User, looser: playerUser, winnerScore: 10, looserScore: 1).save(flush: true, failOnError: true)
        new Game(winner: admin2User, looser: player2User, winnerScore: 10, looserScore: 1).save(flush: true, failOnError: true)

        new Message(author: adminUser, target: playerUser, content: "coucou").save(flush: true, failOnError: true)
        new Message(author: playerUser, target: adminUser, content: "bonsoir").save(flush: true, failOnError: true)
        new Message(author: playerUser, target: adminUser, content: "bonjour", isRead: true).save(flush: true, failOnError: true)

    }

    def destroy = {
    }
}
