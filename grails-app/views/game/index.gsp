<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>
            <a href="#list-game" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
            <div id="list-game" class="content scaffold-list" role="main">
                <h1><g:message code="default.list.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <g:set var="i" value="${0}"/>
                <g:each var="game" in="${gameList}">
                    <g:if test="${i%3 == 0}">
                        <g:set var="className" value="blue"/>
                    </g:if>
                    <g:elseif test="${i%3 == 1}">
                        <g:set var="className" value="red"/>
                    </g:elseif>
                    <g:else>
                        <g:set var="className" value="yellow"/>
                    </g:else>
                    <figure class="snip0078b ${className}">
                        <figcaption>
                            <h2><span>Game ${game.id}</span></h2>
                            <p>Gagnant : ${game.winner.username}</p>
                            <p>Perdant : ${game.looser.username}</p>
                            <p>Score gagnant : ${game.winnerScore}</p>
                            <p>Score perdant : ${game.looserScore}</p>
                            </figcaption>
                            <a href="/tp/game/show/${game.id}"></a>
                        </figure>
                        <g:set var="i" value="${i + 1}"/>
                </g:each>
            </div>
        </div>
    </body>
</html>