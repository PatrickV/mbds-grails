<%@ page import="fr.mbds.tp.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>
            <a href="#create-game" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
            <div id="create-game" class="content scaffold-create" role="main">
                <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.game}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.game}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
                </g:hasErrors>

                <g:form resource="${this.game}" method="POST">
                    <div class="container">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Create Game</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="winner">Winner :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="winner" from="${fr.mbds.tp.User.list(sort:username, order:asc)}" optionKey="id" optionValue="username"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="winnerScore">Winner Score :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:field type="number" name="winnerScore" required="" value="0"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="looser">Looser :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="looser" from="${fr.mbds.tp.User.list(sort:username, order:asc)}" optionKey="id" optionValue="username"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                        <div class="required">
                                            <div class="col-sm-5 col-xs-6 tital">
                                                <label for="looserScore">Looser Score :</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <g:field type="number" name="looserScore" required="" value="0"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer" align="center">
                                    <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </body>
</html>
