<%@ page import="fr.mbds.tp.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-game" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>
            <div id="edit-game" class="content scaffold-edit" role="main">
                <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.game}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.game}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
                </g:hasErrors>
                <g:form resource="${this.game}" method="PUT">
                    <div class="container">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Edit Game</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="winner" width="50%">Winner :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="winner" from="${fr.mbds.tp.User.list(sort: username, order: asc)}" optionKey="id" optionValue="username" value="${this.game.winner.id}"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="winnerScore" width="50%">Winner Score :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:field type="number" name="winnerScore" required="" value="${this.game.winnerScore}"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="looser">Looser :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="looser" from="${fr.mbds.tp.User.list(sort: username, order: asc)}" optionKey="id" optionValue="username" value="${this.game.looser.id}"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="looserScore">Looser Score :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:field type="number" name="looserScore" required="" value="${this.game.looserScore}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer" align="center">
                                    <g:submitButton name="save" class="btn btn-success" value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <g:hiddenField name="version" value="${this.game?.version}" />
                </g:form>
            </div>
        </div>
    </body>
</html>
