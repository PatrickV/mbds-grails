<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-game" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>Resume Game</h4>
                            </div>
                            <div class="panel-body">
                                <div align="center">
                                    <g:link class="edit" action="edit" resource="${this.game}">
                                        <span class="btn btn-info" style="margin-right: 2%">Editer</span>
                                    </g:link>
                                    <span class="btn btn-danger" style="margin-left: 2%" data-toggle="modal" data-target="#game-delete-modal">Supprimer</span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>
                                <hr style="margin: 5px 0 5px 0;">

                                <div class="col-sm-5 col-xs-6 tital">Winner :</div>
                                <div class="col-sm-7"><a href="/tp/user/show/${game.winner.id}">${game.winner.username}</a></div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 tital">Winner Score :</div>
                                <div class="col-sm-7">${game.winnerScore}</div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 tital">Looser :</div>
                                <div class="col-sm-7"><a href="/tp/user/show/${game.looser.id}">${game.looser.username}</a></div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 tital">Looser Score :</div>
                                <div class="col-sm-7">${game.looserScore}</div>
                            </div>

                            <div id="game-delete-modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Suppression de la partie</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Êtes-vous sûr de vouloir supprimer la partie ?</p>
                                            <input type="hidden" id="game-id" name="game-id" value="${game.id}"/>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-sm-5 col-xs-6" align="left">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                            </div>
                                            <div class="col-sm-7" align="right">
                                                <g:form resource="${this.game}" method="DELETE">
                                                    <button type="submit" class="btn btn-danger" id="delete" value="game-delete">Supprimer</button>
                                                </g:form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
