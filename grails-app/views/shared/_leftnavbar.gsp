<nav id="left-panel">
    <h3 class="titre_menu">Users</h3>
    <ul class="puce_menu">
        <li><a href="/tp/user/index" id="user-list" class="lien-non-visible">User List</a></li>
        <li><a href="/tp/user/create" id="user-create" class="lien-non-visible">Create User</a></li>
    </ul>
    <h3 class="titre_menu">Games</h3>
    <ul class="puce_menu">
        <li><a href="/tp/game/index" id="game-list" class="lien-non-visible">Game List</a></li>
        <li><a href="/tp/game/create" id="game-create" class="lien-non-visible">Create Game</a></li>
    </ul>
    <h3 class="titre_menu">Messages</h3>
    <ul class="puce_menu">
        <li><a href="/tp/message/index" id="message-list" class="lien-non-visible">Message List</a></li>
        <li><a href="/tp/message/create" id="message-create" class="lien-non-visible">Create Message</a></li>
    </ul>
</nav>