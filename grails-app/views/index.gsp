<%@ page import="fr.mbds.tp.UserRole; fr.mbds.tp.User" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Dashboard</title>
    <g:set var="nombreUtilisateur" value="${fr.mbds.tp.User.getAll().size()}"/>
    <g:set var="listUserRole" value="${fr.mbds.tp.UserRole.getAll()}"/>
    <g:set var="nombreListUserRole" value="${listUserRole.size()}"/>
    <g:set var="nombreAdmin" value="${0}"/>
    <g:set var ="nombrePlayer" value="${0}"/>
    <g:each var="userRole" in="${listUserRole}">
    <g:if test="${userRole.role.id == 1}">
        <g:set var="nombreAdmin" value="${nombreAdmin+1}"/>
    </g:if>
        <g:else>
            <g:set var="nombrePlayer" value="${nombrePlayer+1}"/>
        </g:else>
    </g:each>
    <g:set var="nombreMessage" value="${fr.mbds.tp.Message.getAll().size()}"/>
    <g:set var="nombrePartie" value="${fr.mbds.tp.Game.getAll().size()}"/>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Utilisateurs', 'Nombre'],
                ['Administrateurs', ${nombreAdmin}],
                ['Joueurs', ${nombrePlayer}]
            ]);

            var options = {
                title: ${nombreUtilisateur} + ' utilisateurs',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
</head>
<body>
    <div id="content">
        <nav id="left-panel">
            <h3 class="titre_menu">Users</h3>
            <ul class="puce_menu">
                <li><a href="./user/index" class="lien-non-visible">User List</a></li>
                <li><a href="./user/create" class="lien-non-visible">Create User</a></li>
            </ul>
            <h3 class="titre_menu">Games</h3>
            <ul class="puce_menu">
                <li><a href="./game/index" class="lien-non-visible">Game List</a></li>
                <li><a href="./game/create" class="lien-non-visible">Create Game</a></li>
            </ul>
            <h3 class="titre_menu">Messages</h3>
            <ul class="puce_menu">
                <li><a href="./message/index" class="lien-non-visible">Message List</a></li>
                <li><a href="./message/create" class="lien-non-visible">Create Message</a></li>
            </ul>
        </nav>
        <content id="page" class="row colset-2-its">

            <sec:ifLoggedIn>
                <h1>Bienvenue</h1>

                <p>Nombre de messages envoyés : ${nombreMessage}</p>
                <p>Nombre de parties jouées : ${nombrePartie}</p>
                <div id="donutchart" style="height: 500px;"></div>

            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <h1>Bienvenue</h1>
                <p>Vous n'etes pas connecté, merci de vous connecter pour acceder au fonctionnalitées de cette platforme</p>
            </sec:ifNotLoggedIn>
        </content>
    </div>

</body>
</html>
