<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <a href="#show-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>

            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>Resume Message</h4>
                            </div>
                            <div class="panel-body">
                                <div align="center">
                                    <g:link class="edit" action="edit" resource="${this.message}">
                                        <span class="btn btn-info" style="margin-right: 2%">Editer</span>
                                    </g:link>
                                    <span class="btn btn-danger" style="margin-left: 2%" data-toggle="modal" data-target="#message-delete-modal">Supprimer</span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>
                                <hr style="margin: 5px 0 5px 0;">

                                <div class="col-sm-5 col-xs-6 tital">Expéditeur :</div>
                                <div class="col-sm-7"><a href="/tp/user/show/${message.author.id}">${message.author.username}</a></div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 tital">Destinataire :</div>
                                <div class="col-sm-7"><a href="/tp/user/show/${message.target.id}">${message.target.username}</a></div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 tital">Contenu :</div>
                                <div class="col-sm-7">${message.content}</div>
                            </div>

                            <div id="message-delete-modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Suppression du message</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Êtes-vous sûr de vouloir supprimer le message ?</p>
                                            <input type="hidden" id="message-id" name="message-id" value="${message.id}"/>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-sm-5 col-xs-6" align="left">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                            </div>
                                            <div class="col-sm-7" align="right">
                                                <g:form resource="${this.message}" method="DELETE">
                                                    <button type="submit" class="btn btn-danger" id="delete" value="message-delete">Supprimer</button>
                                                </g:form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
