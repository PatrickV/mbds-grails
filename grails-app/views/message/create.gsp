<%@ page import="fr.mbds.tp.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<div id="content">
    <g:render template="/shared/leftnavbar"/>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.message}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.message}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <a href="#create-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

    <g:form resource="${this.message}" method="POST">
        <div class="container">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Create Message</h4>
                    </div>
                    <div class="panel-body">
                        <div class="required">
                            <div class="col-sm-5 col-xs-6 tital">
                                <label for='author'>Expéditeur :</label>
                            </div>
                            <div class="col-sm-7">
                                <g:select name="author" from="${fr.mbds.tp.User.list(sort:username, order:asc)}" optionKey="id" optionValue="username"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="bot-border"></div>
                        </div>
                        <div class="required">
                            <div class="col-sm-5 col-xs-7 tital">
                                <label for='target'>Destinataire :</label>
                            </div>
                            <div class="col-sm-7">
                                <g:select name="target" from="${fr.mbds.tp.User.list(sort:username, order:asc)}" optionKey="id" optionValue="username"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="bot-border"></div>

                            <div class="required">
                                <div class="col-sm-5 col-xs-6 tital">
                                    <label for='content'>Contenu :</label>
                                </div>
                                <div class="col-sm-7">
                                    <g:textArea name="content" id="content" value="" required=""></g:textArea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div align="center">
                            <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                            <!-- <button type="submit" class="btn btn-success" id="save">Créer</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
