<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>
            <a href="#list-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
            <div id="list-message" class="content scaffold-list" role="main">
                <h1><g:message code="default.list.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <g:set var="i" value="${0}"/>
                <g:each var="message" in="${messageList}">
                    <g:if test="${i%3 == 0}">
                        <g:set var="className" value="blue"/>
                    </g:if>
                    <g:elseif test="${i%3 == 1}">
                        <g:set var="className" value="red"/>
                    </g:elseif>
                    <g:else>
                        <g:set var="className" value="yellow"/>
                    </g:else>
                    <figure class="snip0078b ${className}">
                        <figcaption>
                            <h2><span>Expéditeur : ${message.author.username}</span></h2>
                            <p>Destinataire : ${message.target.username}</p>
                            <p>Contenu : ${message.content}</p>
                        </figcaption>
                        <a href="/tp/message/show/${message.id}"></a>
                    </figure>
                    <g:set var="i" value="${i + 1}"/>
                </g:each>

                %{--<f:table collection="${messageList}" />--}%

            </div>
        </div>
    </body>
</html>