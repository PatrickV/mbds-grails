<%@ page import="fr.mbds.tp.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="content">
            <g:render template="/shared/leftnavbar"/>
            <div id="edit-message" class="content scaffold-edit" role="main">
                <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.message}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.message}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
                </g:hasErrors>

                <g:form resource="${this.message}" method="PUT">
                    <g:hiddenField name="version" value="${this.message?.version}" />
                    <div class="container">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Edit Message</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="author">Expéditeur :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="author" from="${fr.mbds.tp.User.list(sort:username, order:asc)}" optionKey="id" optionValue="username" value="${this.message.author.id}"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="target">Destinataire :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:select name="target" from="${User.list(sort:username, order: asc)}" optionKey="id" optionValue="username" value="${this.message.target.id}"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                    </div>
                                    <div class="required">
                                        <div class="col-sm-5 col-xs-6 tital">
                                            <label for="content">Contenu :</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <g:textArea name="content" id="content" value="${this.message.content}" required=""></g:textArea>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div align="center">
                                        <g:submitButton name="save" class="btn btn-success" value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </body>
</html>
