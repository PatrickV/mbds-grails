<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="content">

            <g:render template="/shared/leftnavbar"/>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">  <h4 >User Profile</h4></div>
                            <div class="panel-body">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <div class="col-sm-6">
                                            <div  align="center">
                                                <img alt="User Pic" src="${grailsApplication.config.getProperty("fileUrl")+"/"+user.username+"/"+user.profilePicture}" id="profile-image-show" class="img-circle img-responsive"/>
                                            </div>
                                            <br>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-sm-6 show-username">
                                            <span><h4 style="color:#00b1b1; margin-bottom: 5%">${user.username}</h4></span>
                                            <g:link class="edit" action="edit" resource="${this.user}"><span class="btn btn-info" style="margin-right: 2%">Editer</span></g:link><span class="btn btn-danger" style="margin-left: 2%" data-toggle="modal" data-target="#user-delete-modal">Supprimer</span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr style="margin:5px 0 5px 0;">

                                        <div class="col-sm-5 col-xs-6 tital " >Date last connexion :</div><div class="col-sm-7">${user.dateLastConnexion}</div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Account locked :</div><div class="col-sm-7">${user.accountLocked}</div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Account expired :</div><div class="col-sm-7">${user.accountExpired}</div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Password expired :</div><div class="col-sm-7">${user.passwordExpired}</div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Account enable :</div><div class="col-sm-7">${user.enabled}</div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Match won :</div><div class="col-sm-7">
                                        <g:each in = "${user.matchWon.id}">
                                            <a href="/tp/game/show/${it}">Game ${it}</a>
                                        <br />
                                        </g:each>
                                            %{--<a href="/tp/user/show/${user.matchWon.id}">Game ${user.matchWon.id}</a>--}%
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Message recived :</div>
                                        <div class="col-sm-7">
                                            <g:each in = "${user.messageRecived}">
                                                <a href="/tp/message/show/${it.id}">Recived from ${it.author.username}</a>
                                                <br />
                                            </g:each>

                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >Message sent :</div>
                                        <div class="col-sm-7">
                                            <g:each in = "${user.messageSent}">
                                                <a href="/tp/message/show/${it.id}">Sent to ${it.author.username}</a>
                                                <br />
                                            </g:each>
                                        </div>

                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>

                            <div id="user-delete-modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Suppression de ${user.username}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Etes vous sûr de vouloir supprimer l'utilisateur ${user.username} ?</p>
                                            <input type="hidden" id="user-id" name="user-id" value="${user.id}"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                            <button type="button" class="btn btn-danger" id="user-delete">Supprimer</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
