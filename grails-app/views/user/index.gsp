<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="content">

            <g:render template="/shared/leftnavbar"/>

            <a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
            <div id="list-user" class="content scaffold-list" role="main">
                <h1><g:message code="default.list.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
            %{--<f:table collection="${userList}" />--}%

                <g:set var="i" value="${0}"/>
                <g:each var="user" in="${userList}">
                    <g:if test="${user.enabled == true}">
                    <g:if test="${i%3 == 0}">
                        <g:set var="className" value="blue"/>
                    </g:if>
                    <g:elseif test="${i%3 == 1}">
                        <g:set var="className" value="red"/>
                    </g:elseif>
                    <g:else>
                        <g:set var="className" value="yellow"/>
                    </g:else>
                    <figure class="snip0078 ${className}">
                        <div class="card-img">
                            <img src="${grailsApplication.config.getProperty("fileUrl")}/${user.username}/${user.profilePicture}" alt="avatar" />
                        </div>
                        <figcaption>
                            <h2><span>${user.username}</span></h2>
                            <p>Parties gagnées : ${userGames.count {
                                it.winner == user
                            } }</p>
                            <p>Parties perdues : ${userGames.count {
                                it.looser == user
                            } }</p>
                            <p>Messages non lus : ${userMessages.count {
                                it.target == user && it.isRead == false
                            }}</p>
                        </figcaption>
                        <a href="/tp/user/show/${user.id}"></a>
                    </figure>
                    <g:set var="i" value="${i + 1}"/>
                    </g:if>
                </g:each>

            %{--<div class="pagination">
                <g:paginate total="${userCount ?: 0}" />
            </div>--}%
            </div>
        </div>
    </body>
</html>