<%@ page import="fr.mbds.tp.Role" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <asset:stylesheet src="drag.css"/>
</head>
<body>
<a href="#create-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="content">
    <g:render template="/shared/leftnavbar"/>
    <div id="create-user" class="content scaffold-create" role="main">
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

        <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <form action="/tp/user/save" method="post">
            <div class="container">
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <h4>Create User</h4>
                        </div>


                        <div class="panel-body">
                            <div class="col-md-offset-3 col-md-6">
                                <div  align="center" id="dropfile">
                                    <img alt="User Pic" src="${grailsApplication.config.getProperty("fileUrl")+"/"+user.username+"/"+user.profilePicture}" id="profile-image" class="img-circle img-responsive">
                                    <input id="profile-image-upload" class="hidden" type="file">
                                    <div style="color:#999;" >Cliquer sur l'image pour la changer<br />(vous pouvez aussi glisser déposer la nouvelle image dessus !)</div>
                                    <!--Upload Image Js And Css-->
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="bot-border"></div>
                            <div class='required'>
                                <div class="col-sm-5 col-xs-6 tital">
                                    <label for='username'>Username
                                        <span class='required-indicator'>*</span>
                                    </label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" name="username" value="" required="" id="username" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>
                            </div>
                            <div class='required'>
                                <div class="col-sm-5 col-xs-6 tital">
                                    <label for='password'>Password
                                        <span class='required-indicator'>*</span>
                                    </label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="password" name="password" required="" value="" id="password" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>
                            </div>
                            <div class="col-sm-5 col-xs-6 tital">
                                <label for='roles-user'>Role</label>
                            </div>
                            <div class="col-sm-7">
                                <input type="hidden" name="_roles-user" />
                                <input type="checkbox" name="roles-user" value="1" id="roles-user"  /> ROLE_ADMIN<br>
                                <input type="hidden" name="_roles-user" /><input type="checkbox" name="roles-user" value="2" id="roles-user"  /> ROLE_USER<br>
                            </div>
                            <div class='fieldcontain required'>
                                <label for='profilePicture'>Profile Picture
                                    <span class='required-indicator'>*</span>
                                </label><input type="text" name="profilePicture" value="" required="" id="profilePicture" />
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div align="center">
                                <input type="submit" name="create" class="btn btn-success" value="Create" id="create" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
