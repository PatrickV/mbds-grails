<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<a href="#edit-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="content">

    <g:render template="/shared/leftnavbar"/>

    <div id="edit-user" class="content scaffold-edit" role="main">
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:form resource="${this.user}" method="PUT">
            <div class="container">
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Edit User</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-offset-3 col-md-6">
                                <div  align="center" id="dropfile">
                                    <img alt="User Pic" src="${grailsApplication.config.getProperty("fileUrl")+"/"+user.username+"/"+user.profilePicture}" id="profile-image" class="img-circle img-responsive">
            <input id="profile-image-upload" class="hidden" type="file">
            <div style="color:#999;" >Cliquer sur l'image pour la changer<br />(vous pouvez aussi glisser déposer la nouvelle image dessus !)</div>
            <!--Upload Image Js And Css-->
            </div>
            <br>
            <!-- /input-group -->
            </div>
            <div class="clearfix"></div>
            <div class="bot-border"></div>

            <g:hiddenField name="version" value="${this.user?.version}" />
                <div class='required'>
                    <div class="col-sm-5 col-xs-6 tital">
                        <label for='username'>Username<span class='required-indicator'>*</span></label>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" name="username" value="${this.user.username}" required="" id="${this.user.username}" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                </div>
                <div class='required'>
                    <div class="col-sm-5 col-xs-6 tital">
                        <label for='password'>Password<span class='required-indicator'>*</span></label>
                    </div>
                    <div class="col-sm-7">
                        <input type="password" name="password" required="" value="${this.user.password}" id="password" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                </div>
                <div class='col-sm-5 col-xs-6 tital'>
                    <label for='passwordExpired'>Password Expired</label>
                </div>
                <div class="col-sm-7">
                    <input type="hidden" name="_passwordExpired" /><input type="checkbox" name="passwordExpired" id="passwordExpired" <g:if test="${this.user.passwordExpired==true}">checked</g:if> />
                </div>
                <div class="clearfix"></div>
                <div class="bot-border"></div>
                <div class='fieldcontain required'>
                    <label for='profilePicture'>Profile Picture
                        <span class='required-indicator'>*</span>
                    </label><input type="text" name="profilePicture" value="${this.user.profilePicture}" required="" id="profilePicture" />
                </div>
                <div class='col-sm-5 col-xs-6 tital'>
                    <label for='accountExpired'>Account Expired</label>
                </div>
                <div class="col-sm-7">
                    <input type="hidden" name="_accountExpired" /><input type="checkbox" name="accountExpired" id="accountExpired" <g:if test="${this.user.accountExpired==true}">checked</g:if>} />
                </div>
                <div class="clearfix"></div>
                <div class="bot-border"></div>
                <div class='col-sm-5 col-xs-6 tital'>
                    <label for='accountLocked'>Account Locked</label>
                </div>
                <div class="col-sm-7">
                    <input type="hidden" name="_accountLocked" /><input type="checkbox" name="accountLocked" id="accountLocked"  <g:if test="${this.user.accountLocked==true}">checked</g:if> />
                </div>
                <div class="clearfix"></div>
                <div class="bot-border"></div>
                <div class='col-sm-5 col-xs-6 tital'>
                    <label for='enabled'>Enabled</label>
                </div>
                <div class="col-sm-7">
                    <input type="hidden" name="_enabled" /><input type="checkbox" name="enabled" <g:if test="${this.user.enabled==true}">checked</g:if> id="enabled"  />
                </div>
                <div class="clearfix"></div>
                <div class="bot-border"></div>
            <div class="col-sm-5 col-xs-6 tital">
                <label for='roles-user'>Role</label>
            </div>
            <div class="col-sm-7">
                <input type="hidden" name="_roles-user" />
                <input type="checkbox" name="roles-user" value="1" id="roles-user"  /> ROLE_ADMIN<br>
                <input type="hidden" name="_roles-user" /><input type="checkbox" name="roles-user" value="2" id="roles-user" /> ROLE_USER<br>
            </div>
            </div>
                <div class="panel-footer">
                    <div align="center">
                        <input class="btn btn-success" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    </div>
                </div>

        </g:form>
        <input type="hidden" id="username-show" value="${this.user.username}"/>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>
</body>
</html>
