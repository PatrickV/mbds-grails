##Grails - Partie Web app
1. Configuration apache pour l'enregistrement des images sur le serveur apache :
    > Il faut changer le **filePath** en bas de application.yml sans mettre le dernier /
2. Configuration apache pour la récupération des images sur le serveur :
    > Il faut changer le **fileUrl** en bas de application.yml sans mettre le dernier /
3. Creation des avatars pour les utilisateurs existants :
    > Il faut **créer un dossier avec le nom de l'utilisateur** sur le serveur apache puis mettre son avatar avant de modifier l'image de l'utilisateur dans le fichier **BootStrap**
4. Création du dossier temporaire pour les images :
    > Il faut créer un dossier **tmp** au même niveau que les dossiers des utilisateurs sur le serveur apache.    
5. Connexion à l'interface web :
    > il est possible de se connecter avec l'administrateur dont le login est : **admin** et le password est : **password**
- Bonus réalisés :
    1. Envoie des avatars via drag 'n' drop et ajax (dans **drag.js**)
    2. Cron qui supprime les messages lus toutes les nuits à 4h (dans **DailyMessageRemovalService**)
##Grails - Partie REST
- Execution des tests avec Postman :
    1. Executer le test de login pour récuperer les crédentials
    2. Executer la collection pour réaliser l'intégralité des tests
- Documentation :
    1. La documentation de l'API se trouve dans le document **Actions possibles sur les ressources.pdf**
- Bonus réalisés :
    1. Implémentation de **Spring Security Rest**
        > Pour ce faire il a fallut ajouter **'org.grails.plugins:spring-security-rest:2.0.0.RC1'** dans **build.gradle** ainsi que **grails.plugin.springsecurity.rest.token.storage.jwt.secret** dans **application.groovy**, puis lors du login sur l'api, on récupere un token qui permet de s'identifier lors de chaque requête

